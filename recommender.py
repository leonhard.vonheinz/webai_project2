# Contains parts from: https://flask-user.readthedocs.io/en/latest/quickstart_app.html

from flask import Flask, render_template, request, url_for
from flask_user import login_required, UserManager, current_user

from models import db, User, Movie, Rating
from read_data import check_and_read_data

import traceback

# Recommender Class and functions import
from reco_class import *

# import sleep from python
from time import sleep

from sklearn.metrics.pairwise import linear_kernel, cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.impute import SimpleImputer

# Class-based application configuration
class ConfigClass(object):
    """ Flask application config """

    # Flask settings
    SECRET_KEY = 'This is an INSECURE secret!! DO NOT use this in production!!'

    # Flask-SQLAlchemy settings
    SQLALCHEMY_DATABASE_URI = 'sqlite:///movie_recommender.sqlite'  # File-based SQL database
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # Avoids SQLAlchemy warning

    # Flask-User settings
    USER_APP_NAME = "Movie Recommender"  # Shown in and email templates and page footers
    USER_ENABLE_EMAIL = False  # Disable email authentication
    USER_ENABLE_USERNAME = True  # Enable username authentication
    USER_REQUIRE_RETYPE_PASSWORD = True  # Simplify register form

    # make sure we redirect to home view, not /
    # (otherwise paths for registering, login and logout will not work on the server)
    USER_AFTER_LOGIN_ENDPOINT = 'home_page'
    USER_AFTER_LOGOUT_ENDPOINT = 'home_page'
    USER_AFTER_REGISTER_ENDPOINT = 'home_page'
    USER_AFTER_CONFIRM_ENDPOINT = 'home_page'

# Create Flask app
app = Flask(__name__)
app.config.from_object(__name__ + '.ConfigClass')  # configuration
app.app_context().push()  # create an app context before initializing db
db.init_app(app)  # initialize database
db.create_all()  # create database if necessary
user_manager = UserManager(app, db, User)  # initialize Flask-User management

@app.cli.command('initdb')
def initdb_command():
    global db
    """Creates the database tables."""
    check_and_read_data(db)
    print('Initialized the database.')

# Initialize the recommender with movie descriptions as content features
def content_based_recommendation(user_id):
    # Get user's ratings
    user_ratings = Rating.query.filter_by(user_id=user_id).all()

    # Build movie descriptions and ratings dataframe #TODO this is a dict rn, and we don't have descriptions
    movie_descriptions = [movie.description for movie in Movie.query.all()]
    ratings_data = {'movie_id': [], 'rating': []}

    for rating in user_ratings:
        ratings_data['movie_id'].append(rating.movie_id)
        ratings_data['rating'].append(rating.rating)

    #TODO TfidVectorizer und linear_kernel import
    # Create a TfidfVectorizer to convert movie descriptions into a matrix of TF-IDF features
    tfidf_vectorizer = TfidfVectorizer(stop_words='english')
    tfidf_matrix = tfidf_vectorizer.fit_transform(movie_descriptions)

    # Create a linear kernel to calculate cosine similarity
    cosine_sim = linear_kernel(tfidf_matrix, tfidf_matrix)

    # Initialize movie recommendations
    movie_recommendations = []

    # Iterate over user's rated movies
    for i in range(len(ratings_data['movie_id'])):
        movie_id = ratings_data['movie_id'][i]

        # Get the index of the movie in the descriptions list
        idx = Movie.query.get(movie_id).id - 1

        # Calculate the similarity scores for all movies
        sim_scores = list(enumerate(cosine_sim[idx]))

        # Sort movies based on similarity scores
        sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)

        # Get the top 5 similar movies (excluding the rated ones)
        top_movies = [sim_score[0] for sim_score in sim_scores[1:6] if sim_score[0] not in ratings_data['movie_id']]

        # Add recommendations to the list
        movie_recommendations.extend(top_movies)

    # Return the top 10 recommended movies
    return Movie.query.filter(Movie.id.in_(movie_recommendations)).all()

# Collaborative Filtering: Item-Item Similarity
def item_item_collaborative_filtering(user_id):
    # Get user's ratings
    user_ratings = Rating.query.filter_by(user_id=user_id).all()

    # Build user-item matrix
    user_item_matrix = []
    for movie in Movie.query.all():
        movie_ratings = [rating.rating if rating.movie_id == movie.id else None for rating in user_ratings]
        user_item_matrix.append(movie_ratings)

    user_item_matrix = SimpleImputer().fit_transform(user_item_matrix)  # Handle missing values

    # Calculate item-item similarity
    item_similarity = cosine_similarity(user_item_matrix.T)

    # Identify movies not rated by the user
    unrated_movies = [i for i, rating in enumerate(user_item_matrix[0]) if rating is None]

    # Sum the similarity scores for each unrated movie
    movie_scores = [(movie_id, sum(item_similarity[movie_id][rated_movie] for rated_movie in range(len(user_item_matrix[0])))) for movie_id in unrated_movies]

    # Sort movies by score in descending order
    recommended_movies = sorted(movie_scores, key=lambda x: x[1], reverse=True)

    # Return the top 10 recommended movies
    return Movie.query.filter(Movie.id.in_([movie[0] for movie in recommended_movies[:10]])).all()

# favicon for browsers
@app.route("/favicon.ico")
def favicon():
    return url_for('static', filename="favicon.ico")

# The Home page is accessible to anyone
@app.route('/')
def home_page():
    # render home.html template
    return render_template("home.html")


# The Members page is only accessible to authenticated users via the @login_required decorator
@app.route('/movies')
@login_required  # User must be authenticated
def movies_page():
    # String-based templates

    # first 10 movies
    movies = Movie.query.limit(10).all()

    # only Romance movies
    # movies = Movie.query.filter(Movie.genres.any(MovieGenre.genre == 'Romance')).limit(10).all()

    # only Romance AND Horror movies
    # movies = Movie.query\
    #     .filter(Movie.genres.any(MovieGenre.genre == 'Romance')) \
    #     .filter(Movie.genres.any(MovieGenre.genre == 'Horror')) \
    #     .limit(10).all()

    return render_template("movies.html", movies=movies)


@app.route('/rate', methods=['POST'])
@login_required  # User must be authenticated
def rate():

    movieid = request.form.get('movieid')
    rating = request.form.get('rating')
    userid = current_user.id
    print("Rate {} for {} by {}".format(rating, movieid, userid))
    return render_template("rated.html", rating=rating)


@app.route('/recommend', methods=['POST'])
@login_required  # User must be authenticated
def recommend():
    movies = Movie.query.limit(10).all()
    return render_template("recommendations.html", movies=movies)

@app.errorhandler(500)
def internal_error(exception):
   return "<pre>"+traceback.format_exc()+"</pre>"

# Start development web server
if __name__ == '__main__':
    app.run(port=5000, debug=True)
