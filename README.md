# WEB-AI Movie recommender system

This is Group 19's Project 2 **Movie recommender system**.

## Authors and acknowledgment
Alexander Gerhard Ditz

Nils Niehaus

Leonard von Heiz

## Description

This 'python' project consists of:

- A **Flask app** 

## Requirements

You need a 'python' environment with the packages:
- flask
- flask-sqlalchemy
- flask-user==1.0.2.2
- scikit-learn
- scikit-surprise
installed,
as well as an internet connection.

## Usage

To use the recommender app from your local directory, **run 'flask --app recommender run'** in the terminal.


## Features

Function 'content_basesd_recommendation()' creates a linear kernel to map 


## Implementation


## Gitlab

https://gitlab.gwdg.de/leonhard.vonheinz/webai_project2


## TODOS

- display avg ratings
- save ratings to db
- searchable tags, searchable titles?
- no duplicate tags
- rank tags in search function
- display user history/ratings (used as recommendation basis)
